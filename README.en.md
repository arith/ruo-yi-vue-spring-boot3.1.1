# RuoYi-Vue-SpringBoot3.1.1

#### Description
RuoYi-Vue 项目升级到 SpringBoot3.1.1, JDK17
只包含后端代码。

方便需要这个版本的同学下载。

或者有兴趣的也可以一起维护这个版本，欢迎 issue。

### 升级主要涉及的模块及代码
* javax.servlet-api -> jakarta.servlet-api，及相关的引用包名更改
* druid-spring-boot-starter -> druid-spring-boot-3-starter，及相关的引用包名更改
* druid 适配 springboot3, 在 ruoyi-admin 的 resource 文件夹下 META-INF 下新建spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports
* SecurityConfig 配置更改
* mysql 驱动：mysql-connector-java -> mysql-connector-j
* spring6 版本的 core 包移除了 NestedIOException，所以添加了 NestedIOException。
* springboot 版本修改等。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
