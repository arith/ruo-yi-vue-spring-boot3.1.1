# RuoYi-Vue-SpringBoot3.1.1

#### 介绍
RuoYi-Vue 项目(版本：3.8.6)升级到 SpringBoot3.1.1, JDK17
只包含后端代码。

方便需要这个版本的同学下载。

或者有兴趣的也可以一起维护这个版本，欢迎 issue。

### 升级主要涉及的模块及代码
* javax.servlet-api -> jakarta.servlet-api，及相关的引用包名更改
* druid-spring-boot-starter -> druid-spring-boot-3-starter，及相关的引用包名更改
* druid 适配 springboot3, 在 ruoyi-admin 的 resource 文件夹下 META-INF 下新建spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports
* SecurityConfig 配置更改
* mysql 驱动：mysql-connector-java -> mysql-connector-j
* spring6 版本的 core 包移除了 NestedIOException，所以添加了 NestedIOException。
* springboot 版本修改等。


#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
